Vi Passei Tech Challenge
========================

# 👓 Introdução


![status_desenvolvimento](https://img.shields.io/static/v1?label=Status&message=Em%20Desenvolvimento&color=yellow&style=for-the-badge)


![Badge Java](https://img.shields.io/static/v1?label=Java&message=17&color=orange&style=for-the-badge&logo=java)


![framework_back](https://img.shields.io/badge/Spring_Boot-F2F4F9?style=for-the-badge&logo=spring-boot)
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)

![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![GitLab CI](https://img.shields.io/badge/gitlab%20ci-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)


[![pipeline status](https://gitlab.com/vsantsal/vi-passei-tech-challenge/badges/main/pipeline.svg)](https://gitlab.com/vsantsal/vi-passei-tech-challenge/-/commits/main)
[![coverage report](https://gitlab.com/vsantsal/vi-passei-tech-challenge/badges/main/coverage.svg)](https://gitlab.com/vsantsal/vi-passei-tech-challenge/-/commits/main)


Repositório de projeto com desenvolvimento de aplicação web (*backend*) de streaming de vídeos permitindo o gerenciamento e exibição de vídeos, focado em conteúdo de aulas de matérias cobradas em concursos públicos (*direito administrativo*, *direito constitucional*, *raciocínio lógico e matemático* etc).

O link no *Gitlab* é https://gitlab.com/vsantsal/vi-passei-tech-challenge.

# Como executar

Para executar o projeto, deve-se definir valores convenientes para as seguintes variáveis de ambiente utilizadas nas configurações do projeto:

* `MONGO_URL`
* `JWT_TOKEN`
* `JWT_ISSUER`

# 📖 APIs

Abaixo, descrevemos globalmente as funcionalidades implementadas.

## Autenticar Usuário

Nossa API Rest deve suportar cadastro e posterior login para usuários, disponíveis nos *endpoints* `auth/registrar` e `auth/login`, respectivamente.

Para o POST em `auth/registrar`, o *body* de cada requisição deve informar JSON no seguinte formato:

```json
{
    "login": "username",
    "senha": "password",
    "papel": "ALUNO"
}

```

Apenas usuários já cadastrados e com perfil de administrador podem registrar outros usuários.

Em caso de cadastro bem sucedido, a aplicação retorna resposta com status HTTP usual (200).

Caso haja nova tentativa de cadastro, a aplicação retornará o erro informando, conforme abaixo:

```json
{
    "mensagem": "Login 'username' já utilizado"
}
```

Para o POST em `auth/login`, o *body* de cada requisição deve informar JSON no seguinte formato:

```json
{
    "login": "username",
    "senha": "password"
}
```
Em caso de login inválido, a aplicação retorna o status 403 (sem mensagem).

Em caso de login bem sucedido, a aplicação retornará token JWT que o cliente deverá informar a cada nova solicitação.

## Vídeoaulas 

Nossa API Rest deve suportar a manutenção do cadastro de vídeos para as aulas.

O enpdpoint será baseado em `/videoaulas`, suportando os métodos HTTP GET, POST, PUT, DELETE.

Há restrições conforme as permissões dos usuários, conforme as regras a seguir:
* Um `aluno` somente pode visualizar vídeos específicos ou listar vídeos disponíveis em pesquisas;
* Um `professor`, além das permissões disponíveis aos `alunos`, pode cadastrar vídeos na plataforma, além de editá-los;
* Um `administrador`, além das permissões disponíveis a `alunos` e `professores`, pode remover vídeos da plataforma.

Para o POST, o *body* de cada requisição deve informar JSON no seguinte formato:

```json
{
  "titulo": "Aula de Design Patterns",
  "descrico": "Aprofundando para discursivas",
  "url": "https://www.vipassei.com.br/auladesignpatterns"
}
```

A resposta da requisição ocorre como no exemplo abaixo (STATUS CODE 201)

```json
{
  "id": "654968c2aca41a551bb22ca4",
  "titulo": "Aula de Design Patterns",
  "descricao": "Aprofundando para discursivas",
  "url": "https://www.vipassei.com.br/auladesignpatterns",
  "dataPublicacao": "2023-11-06T19:29:22.898321293"
}
```

Caso a requisição contenha dados inválidos (campo ausente, URL inválida etc.), há tratamentos de erro, conforme exemplos abaixo.

```json
[
    {
        "campo": "titulo",
        "mensagem": "Deve-se informar título para o vídeo"
    }
]
```

```json
[
    {
        "campo": "descricao",
        "mensagem": "Deve-se informar descrição para o vídeo"
    }
]
```

```json
[
    {
        "campo": "url",
        "mensagem": "URL deve ser válida"
    }
]
```

O GET pode ser realizado com ou sem passagem de id, para retornar um vídeo específico ou uma listagem, respectivamente.

A listagem é paginada e permite a ordenação por data de Publicação. É possível filtrar por título e data de publicação (informada como *LocalDate* e não *LocalDateTime*).

Caso não seja encontrada visualização correspondente na listagem, a API retorna erro com código 404 e a mensagem abaixo:

```json
{
  "mensagem": "Não há vídeos disponíveis para visualização"
}
```

Se não houver resultado na visualização específica de um vídeo, a mensagem é mais indicativa.

Para o DELETE, deve-se passar o id do vídeo a remover no endpoint (por exemplo, `videoaulas/65abcde`). A aplicação marcará o vídeo como inativo (a exclusão é apenas lógica) e retornará o STATUS CODE 204.

```json
{
  "mensagem": "Não foi possível encontrar vídeo com o código 'abcde' informado"
}
```

Para o PUT, deve-se passar o id do vídeo e os novos valores a atualizar:

```json
{
  "titulo": "Aula de Design Patterns 2",
  "descrico": "Aprofundando para discursivas 2",
  "url": "https://www.vipassei.com.br/auladesignpatterns2"
}
```

# 🗓️ Resumo Desenvolvimento

* Para gestão do *backlog* no serviço, utilizamos os [Issue boards](https://gitlab.com/vsantsal/vi-passei-tech-challenge/-/boards) do repositório
* Para criar e executar o *pipeline* de *CI/CD* do *Gitlab*, nos baseamos em: 
  * Tutorial disponibilizado por eles em https://docs.gitlab.com/ee/ci/quick_start/index.html
  * Artigo [Getting started with Gitlab CI/CD for Spring Boot projects on Standalone Gitlab installation](https://medium.com/@pavan.programmer/getting-started-with-gitlab-ci-cd-for-spring-boot-projects-on-standalone-gitlab-installation-3270658d8732)
  * Tutorial de uma hora de *TechWorld with Nana* no Youtube [GitLab CI CD Tutorial for Beginners [Crash Course]](https://www.youtube.com/watch?v=qP8kir2GUgo&t=544s) 
  * Tutorial específico para Java/Jacoco [Code coverage badge on Gitlab using Maven and JaCoCo plugin | Java | JVM](https://www.youtube.com/watch?v=nrs-DjTRwkk)
* Para criar a lógica de autenticação e autorização na aplicação, nos baseamos no tutorial do Youtube de Fernanda Kipper [Autenticação e Autorização com Spring Security e JWT Tokens](https://www.youtube.com/watch?v=5w-YCcOjPD0) e seu respectivo [repositório no Github](https://github.com/Fernanda-Kipper/auth-api);
* Para adicionar variáveis de ambiente no projeto do *Gitlab*, recorremos à sua documentação ([Gitlab CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/));
* Para somente executar os testes quando houvesse *merge request*, ajustamos o `.gitlab-ci.yml` conforme sua documentação ([Merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html));
* Nos baseamos no exemplo de teste parametrizado `NumberUtilsTest` disponível no capítulo 2 *Specification-based testing* de *Effective Software Testing* do Maurício Aniche e seu uso de `@MethodSource` fazendo referência a método estático para geração de massa de testes em `VideoControllerIntegrationTest`;

