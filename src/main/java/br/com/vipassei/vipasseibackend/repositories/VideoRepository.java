package br.com.vipassei.vipasseibackend.repositories;

import br.com.vipassei.vipasseibackend.domain.video.Video;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

public interface VideoRepository extends ReactiveMongoRepository<Video, String> {

    Mono<Video> findByIdAndAtivoTrue(String s);

    Flux<Video> findAllByAtivoTrue(Pageable pageable);
}
