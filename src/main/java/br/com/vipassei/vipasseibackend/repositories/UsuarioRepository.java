package br.com.vipassei.vipasseibackend.repositories;

import br.com.vipassei.vipasseibackend.domain.usuario.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {
    Optional<UserDetails> findByLogin(String login);
}
