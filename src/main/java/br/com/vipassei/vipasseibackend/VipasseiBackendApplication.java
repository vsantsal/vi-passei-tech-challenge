package br.com.vipassei.vipasseibackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VipasseiBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(VipasseiBackendApplication.class, args);
	}

}
