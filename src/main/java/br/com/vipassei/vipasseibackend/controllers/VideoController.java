package br.com.vipassei.vipasseibackend.controllers;

import br.com.vipassei.vipasseibackend.domain.video.VideoDTO;

import br.com.vipassei.vipasseibackend.services.VideoService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/videoaulas")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','PROFESSOR')")
    public Mono<ResponseEntity<VideoDTO>> publicar(
            @RequestBody @Valid VideoDTO dto,
            UriComponentsBuilder uriComponentsBuilder
    ){
        var dtoSalvo = videoService.publicar(dto);
        var uri = uriComponentsBuilder.path("/videoaulas/{id}").buildAndExpand(dtoSalvo.id()).toUri();
        return Mono.just(ResponseEntity.created(uri).body(dtoSalvo));

    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasAnyRole('ADMIN','PROFESSOR', 'ALUNO)")
    public Mono<ResponseEntity<VideoDTO>> visualizar(
            @PathVariable String codigo
    ) throws ExecutionException, InterruptedException {
        var dto = videoService.visualizar(codigo);
        return Mono.just(ResponseEntity.ok(dto));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','PROFESSOR', 'ALUNO)")
    public Flux<ResponseEntity<Page<VideoDTO>>> listar(
            @RequestParam(required = false) String titulo,
            @RequestParam(required = false) LocalDate dataPublicacao,
            @PageableDefault(
                    sort = {"dataPublicacao"},
                    direction = Sort.Direction.DESC
            ) Pageable paginacao
    ){
        LocalDateTime dataPublicacaoDt = dataPublicacao == null ? null : dataPublicacao.atTime(0, 0);
        var paramPesquisa = new VideoDTO(null, titulo, null, "https://wwww", dataPublicacaoDt);
        var pagina = videoService.listar(paramPesquisa, paginacao);
        return Flux.just(ResponseEntity.ok(pagina));
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public Mono<ResponseEntity<String>> desativa(
            @PathVariable String codigo
            ) throws ExecutionException, InterruptedException {
        videoService.remover(codigo);
        return Mono.just(ResponseEntity.noContent().build());
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasAnyRole('ADMIN','PROFESSOR')")
    public Mono<ResponseEntity<VideoDTO>> edita(
            @PathVariable String codigo,
            @RequestBody @Valid VideoDTO dto

    ) throws ExecutionException, InterruptedException {
        var dtoEditado = videoService.editar(codigo, dto);
        return Mono.just(ResponseEntity.ok(dtoEditado));
    }

}
