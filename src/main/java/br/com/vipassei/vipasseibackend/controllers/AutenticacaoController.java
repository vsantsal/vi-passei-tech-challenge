package br.com.vipassei.vipasseibackend.controllers;

import br.com.vipassei.vipasseibackend.domain.usuario.AutenticacaoDTO;
import br.com.vipassei.vipasseibackend.domain.usuario.RegistroDTO;
import br.com.vipassei.vipasseibackend.domain.usuario.RespostaLoginDTO;
import br.com.vipassei.vipasseibackend.domain.usuario.Usuario;
import br.com.vipassei.vipasseibackend.exceptions.UsuarioCadastradoException;
import br.com.vipassei.vipasseibackend.infrastructure.security.TokenService;
import br.com.vipassei.vipasseibackend.repositories.UsuarioRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
public class AutenticacaoController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private TokenService tokenService;

    @PostMapping("/login")
    public ResponseEntity<RespostaLoginDTO> login(@RequestBody @Valid AutenticacaoDTO dto) {
        var usuarioSenha = new UsernamePasswordAuthenticationToken(dto.login(), dto.senha());

        var auth = authenticationManager.authenticate(usuarioSenha);

        var token = tokenService.geraToken((Usuario) auth.getPrincipal());

        return ResponseEntity.ok(new RespostaLoginDTO(token));
    }

    @PostMapping("/registrar")
    public ResponseEntity registrar(@RequestBody @Valid RegistroDTO dto) {
        if (repository.findByLogin(dto.login()).isPresent()) {
            throw new UsuarioCadastradoException("Login '" + dto.login() + "' já utilizado");
        }


        String senhaCriptografada = passwordEncoder.encode(dto.senha());

        var dtoComSenhaEncriptada = new RegistroDTO(dto.login(), senhaCriptografada, dto.papel());
        var novoUsuario = dtoComSenhaEncriptada.toModel();

        repository.save(novoUsuario);

        return ResponseEntity.ok().build();
    }
}
