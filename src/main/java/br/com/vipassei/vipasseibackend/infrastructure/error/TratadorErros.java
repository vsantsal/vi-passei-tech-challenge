package br.com.vipassei.vipasseibackend.infrastructure.error;


import br.com.vipassei.vipasseibackend.exceptions.ConstanteEnumInexistenteException;
import br.com.vipassei.vipasseibackend.exceptions.ResultadoVazioException;
import br.com.vipassei.vipasseibackend.exceptions.UsuarioCadastradoException;
import br.com.vipassei.vipasseibackend.exceptions.VideoNaoEncontradoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TratadorErros {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity tratarErro400ComFieldErros(MethodArgumentNotValidException exception) {
        var erros = exception.getFieldErrors();
        return ResponseEntity.badRequest().body(
                erros.stream().map(DadosErrosValidacao::new).toList()
        );
    }


    @ExceptionHandler({UsuarioCadastradoException.class})
    public ResponseEntity trataErro409(Exception exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(
                new ErroSoComMensagemValidacao(exception.getMessage())
        );
    }

    @ExceptionHandler({ConstanteEnumInexistenteException.class})
    public ResponseEntity trataErro400(Exception exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ErroSoComMensagemValidacao(exception.getMessage())
        );
    }

    @ExceptionHandler({
            VideoNaoEncontradoException.class,
            ResultadoVazioException.class
    })
    public ResponseEntity trataErro404(Exception exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErroSoComMensagemValidacao(exception.getMessage())
        );
    }

    private record ErroSoComMensagemValidacao(String mensagem){ }
    private record DadosErrosValidacao(String campo, String mensagem) {
        public DadosErrosValidacao(FieldError erro) {
            this(erro.getField(), erro.getDefaultMessage());
        }
    }
}
