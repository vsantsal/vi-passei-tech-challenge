package br.com.vipassei.vipasseibackend.exceptions;

public class UsuarioCadastradoException extends RuntimeException {
    public UsuarioCadastradoException(String message) {
        super(message);
    }
}
