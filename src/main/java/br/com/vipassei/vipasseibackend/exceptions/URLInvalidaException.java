package br.com.vipassei.vipasseibackend.exceptions;

public class URLInvalidaException extends RuntimeException {

    public URLInvalidaException(String message) {
        super(message);
    }
}
