package br.com.vipassei.vipasseibackend.exceptions;

public class AutenticacaoException extends RuntimeException{

    public AutenticacaoException(String message) {
        super(message);
    }
}
