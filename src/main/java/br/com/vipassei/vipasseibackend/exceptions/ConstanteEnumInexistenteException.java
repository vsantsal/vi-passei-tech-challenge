package br.com.vipassei.vipasseibackend.exceptions;

public class ConstanteEnumInexistenteException extends RuntimeException {

    public ConstanteEnumInexistenteException(String message) {
        super(message);
    }
}
