package br.com.vipassei.vipasseibackend.exceptions;

public class VideoNaoEncontradoException extends RuntimeException {
    public VideoNaoEncontradoException(String message) {
        super(message);
    }
}
