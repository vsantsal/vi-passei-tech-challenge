package br.com.vipassei.vipasseibackend.exceptions;

public class ResultadoVazioException extends RuntimeException {
    public ResultadoVazioException(String message) {
        super(message);
    }
}
