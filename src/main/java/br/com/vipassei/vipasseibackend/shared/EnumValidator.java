package br.com.vipassei.vipasseibackend.shared;

import br.com.vipassei.vipasseibackend.exceptions.ConstanteEnumInexistenteException;

public class EnumValidator {

    private EnumValidator(){

    }

    public static <E extends Enum<E>> E recupera(Class<E> e, String valor, String campoDTO) {
        try {
            return Enum.valueOf(e, valor);
        } catch (IllegalArgumentException ex) {
            throw new ConstanteEnumInexistenteException(
                    "Valor '" + valor + "' inválido para '" + campoDTO + "'"
            );
        }
    }
}
