package br.com.vipassei.vipasseibackend.domain.video;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.time.LocalDateTime;

@Document
@Getter
public class Video {
    @Id
    private String id;

    @NotBlank(message = "título não pode ser vazio")
    @Size(max = 100, message = "Título deve possuir até 100 caracteres")
    @Setter
    private String titulo;

    @NotBlank(message = "descrição não pode ser vazio")
    @Size(max = 255, message = "Descrição deve possuir até 255 caracteres")
    @Setter
    private String descricao;

    @URL
    private String url;

    private LocalDateTime dataPublicacao;

    private boolean ativo;

    private LocalDateTime dataUltimaAtualizacao;

    /**
      * @deprecated
      * Código cliente não deveria chamar o construtor sem parâmetros de Video.
      * Deixar somente para eventuais necessidade de Frameworks,
      * */
    @Deprecated(forRemoval = false)
    public Video(){
        this.dataPublicacao = LocalDateTime.now();
        this.dataUltimaAtualizacao = dataPublicacao;
        this.ativo = true;
    }

    public Video(@NotBlank String titulo, @NotBlank String descricao, @URL String url){
        this.titulo = titulo;
        this.descricao = descricao;
        this.url = URLValidator.valida(url);
        this.dataPublicacao = LocalDateTime.now();
        this.dataUltimaAtualizacao = this.dataPublicacao;
        this.ativo = true;
    }

    public Video(
            @NotBlank String titulo,
            @NotBlank String descricao,
            @URL String url,
            @NotNull LocalDateTime dataPublicacao
    ){
        this.titulo = titulo;
        this.descricao = descricao;
        this.url = URLValidator.valida(url);
        this.dataPublicacao = dataPublicacao;
        this.dataUltimaAtualizacao = dataPublicacao;
        this.ativo = true;
    }

    public void setUrl(String url) {
        this.url = URLValidator.valida(url);
    }

    public void inativa() {
        if (ativo){
            this.ativo = false;
            this.dataUltimaAtualizacao = LocalDateTime.now();
        }
    }
}
