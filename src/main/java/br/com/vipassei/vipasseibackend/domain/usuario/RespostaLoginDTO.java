package br.com.vipassei.vipasseibackend.domain.usuario;

public record RespostaLoginDTO(String token) {
}
