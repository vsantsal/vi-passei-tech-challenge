package br.com.vipassei.vipasseibackend.domain.video;

import br.com.vipassei.vipasseibackend.exceptions.URLInvalidaException;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class URLValidator {
    /*
    Implementação adaptada de https://www.baeldung.com/java-validate-url.
    */
    public static String valida(String url){
        try {
            new URL(url).toURI();
            return url;
        } catch (MalformedURLException | URISyntaxException ex){
            throw new URLInvalidaException("url '" + url + "' inválida");
        }

    }

    private URLValidator(){}
}
