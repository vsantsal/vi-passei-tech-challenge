package br.com.vipassei.vipasseibackend.domain.usuario;


import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Document
public class Usuario implements UserDetails {
    @Id
    private String id;
    private String login;
    private String senha;


    @Getter
    private PapelUsuario papel;

    public Usuario(String login, String senha, PapelUsuario papel){
        this.login = login;
        this.senha = senha;
        this.papel = papel;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        final String ROLE_ALUNO = "ROLE_ALUNO";
        final String ROLE_PROFESSOR = "ROLE_PROFESSOR";
        final String ROLE_ADMIN = "ROLE_ADMIN";

        if (this.papel == PapelUsuario.ADMIN){
            return List.of(
                    new SimpleGrantedAuthority(ROLE_ADMIN),
                    new SimpleGrantedAuthority(ROLE_PROFESSOR),
                    new SimpleGrantedAuthority(ROLE_ALUNO)
            );
        }
        if (this.papel == PapelUsuario.PROFESSOR){
            return List.of(
                    new SimpleGrantedAuthority(ROLE_PROFESSOR),
                    new SimpleGrantedAuthority(ROLE_ALUNO)
            );
        }
        return List.of(new SimpleGrantedAuthority(ROLE_ALUNO));
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

