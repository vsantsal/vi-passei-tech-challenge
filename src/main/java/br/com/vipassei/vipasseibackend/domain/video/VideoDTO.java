package br.com.vipassei.vipasseibackend.domain.video;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import org.hibernate.validator.constraints.URL;


import java.time.LocalDateTime;

public record VideoDTO(
        String id,

        @NotBlank(message = "Deve-se informar título para o vídeo")
        @Size(max = 100, message = "Título deve possuir até 100 caracteres")
        String titulo,

        @NotBlank(message = "Deve-se informar descrição para o vídeo")
        @Size(max = 255, message = "Descrição deve possuir até 255 caracteres")
        String descricao,

        @NotBlank(message = "Deve-se informar url para o vídeo")
        @URL(message = "URL deve ser válida")
        String url,

        LocalDateTime dataPublicacao

) {
        public VideoDTO(Video video) {
                this(video.getId(), video.getTitulo(),
                        video.getDescricao(), video.getUrl(), video.getDataPublicacao());
        }

        public Video toModel() {
                return new Video(titulo(), descricao(), url());
        }
}
