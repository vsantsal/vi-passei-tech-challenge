package br.com.vipassei.vipasseibackend.domain.usuario;

public enum PapelUsuario {
    ADMIN("admin"), ALUNO("aluno"), PROFESSOR("professor");

    private String papel;

    PapelUsuario(String papel){
        this.papel = papel;
    }

    public String getPapel(){
        return this.papel;
    }

}
