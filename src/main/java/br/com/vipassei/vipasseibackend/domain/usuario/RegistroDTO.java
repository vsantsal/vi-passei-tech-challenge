package br.com.vipassei.vipasseibackend.domain.usuario;

import br.com.vipassei.vipasseibackend.shared.EnumValidator;

public record RegistroDTO(String login, String senha, String papel) {

    public Usuario toModel(){
        return  new Usuario(login(), senha(), EnumValidator.recupera(PapelUsuario.class, papel(), "papel"));
    }
}
