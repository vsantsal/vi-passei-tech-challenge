package br.com.vipassei.vipasseibackend.domain.usuario;

public record AutenticacaoDTO(String login, String senha) {
}
