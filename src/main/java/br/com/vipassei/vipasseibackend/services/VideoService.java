package br.com.vipassei.vipasseibackend.services;

import br.com.vipassei.vipasseibackend.domain.video.VideoDTO;
import br.com.vipassei.vipasseibackend.exceptions.ResultadoVazioException;
import br.com.vipassei.vipasseibackend.exceptions.VideoNaoEncontradoException;
import br.com.vipassei.vipasseibackend.repositories.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
public class VideoService {
    @Autowired
    private VideoRepository videoRepository;

    @Transactional
    public VideoDTO publicar(VideoDTO dto) {
        var videoASalvar = dto.toModel();
        videoRepository.save(videoASalvar);
        return  new VideoDTO(videoASalvar);
    }

    @Transactional(readOnly = true)
    public VideoDTO visualizar(String codigo) throws ExecutionException, InterruptedException {
        var videoAVisualizar = videoRepository.findByIdAndAtivoTrue(codigo).toFuture().get();
        if (videoAVisualizar == null){
            throw new VideoNaoEncontradoException(
                    "Não foi possível encontrar vídeo com o código '" + codigo + "' informado");
        }

        return new VideoDTO(videoAVisualizar);
    }

    @Transactional(readOnly = true)
    public Page<VideoDTO> listar(VideoDTO dto, Pageable paginacao) {
        var videos = videoRepository
                .findAllByAtivoTrue(paginacao)
                .toStream()
                .filter(v -> (dto.titulo() == null || dto.titulo().equals(v.getTitulo())))
                .filter(v -> (dto.dataPublicacao() == null || dto.dataPublicacao().toLocalDate().equals(v.getDataPublicacao().toLocalDate())))
                .map(VideoDTO::new)
                .toList();
        if (videos.isEmpty()) {
            throw new ResultadoVazioException("Não há vídeos disponíveis para visualização");
        }
        return new PageImpl<>(videos, paginacao, videos.size());
    }

    public boolean remover(String codigo) throws ExecutionException, InterruptedException {
        var videoAExcluirOptional = this.videoRepository.findByIdAndAtivoTrue(codigo).toFuture().get();
        // Caso não encontre vídeo a exluir logicamente, service sinaliza com marcação de falso
        if (videoAExcluirOptional == null){
            throw new VideoNaoEncontradoException(
                    "Não foi possível encontrar o vídeo de id '" +
                            codigo +
                            "'"
            );
        }
        // Inativa, salva o vídeo atualizado no repositório e
        // sinaliza ao código cliente o sucesso da operação (veradeiro)
        var videoAExcluir = videoAExcluirOptional;
        videoAExcluir.inativa();
        this.videoRepository.save(videoAExcluir);
        return true;
    }

    public VideoDTO editar(String codigo, VideoDTO dto) throws ExecutionException, InterruptedException {

        var videoAEditarOptional = this.videoRepository.findByIdAndAtivoTrue(codigo).toFuture().get();
        if (videoAEditarOptional == null){
            throw new VideoNaoEncontradoException(
                    "Não foi possível encontrar o vídeo de id '" +
                            codigo +
                            "'"
            );
        }

        var videoAEditar = videoAEditarOptional;
        videoAEditar.setTitulo(dto.titulo());
        videoAEditar.setDescricao(dto.descricao());
        videoAEditar.setUrl(dto.url());

        videoRepository.save(videoAEditar);

        return new VideoDTO(videoAEditar);

    }
}
