package br.com.vipassei.vipasseibackend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VipasseiBackendApplicationTest {

    @Test
    void contextLoad(){
        assertDoesNotThrow(() -> VipasseiBackendApplication.main(new String[] {}));
    }

}