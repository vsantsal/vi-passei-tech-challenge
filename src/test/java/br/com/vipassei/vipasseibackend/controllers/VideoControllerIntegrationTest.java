package br.com.vipassei.vipasseibackend.controllers;

import br.com.vipassei.vipasseibackend.domain.usuario.PapelUsuario;
import br.com.vipassei.vipasseibackend.domain.usuario.Usuario;
import br.com.vipassei.vipasseibackend.domain.video.Video;
import br.com.vipassei.vipasseibackend.repositories.UsuarioRepository;
import br.com.vipassei.vipasseibackend.repositories.VideoRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class VideoControllerIntegrationTest {

    private final String ENDPOINT = "/videoaulas";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @AfterEach
    public void tearDown(){
        mongoTemplate.getDb().drop();
    }

    @DisplayName("Testa aluno não pode publicar video")
    @Test
    void naoDevePublicarVideoSeAluno() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAluno();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Minha própria aula\", " +
                                                "\"descricao\": \"não preciso de professor\", " +
                                                "\"url\": \"http://www.meuvideo.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa usuário com permissão pode publicar video")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoPOSTePUT")
    void devePublicarVideoSeProfessorOuAdmin(
            String login,
            String senha,
            PapelUsuario papel
    ) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(request().asyncStarted())
                .andReturn();
        ;

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"));
    }

    @DisplayName("Testa não pode publicar vídeo sem título")
    @Test
    void naoDevePublicarVideoSeNaoTiverTitulo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioProfessor();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                        "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("titulo")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("Deve-se informar título para o vídeo")))
        ;
    }

    @DisplayName("Testa não pode publicar vídeo sem descrição")
    @Test
    void naoDevePublicarVideoSeNaoTiverDescricao() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"titulo\": \"Noções de Direito Constitucional\", " +
                                        "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("descricao")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("Deve-se informar descrição para o vídeo")))
        ;
    }

    @DisplayName("Testa não pode publicar vídeo sem url")
    @Test
    void naoDevePublicarVideoSeNaoTiverURL() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"titulo\": \"Noções de Direito Constitucional\", " +
                                        "\"descricao\": \"Indo até detalhes de jurisprudências\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("url")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("Deve-se informar url para o vídeo")))
        ;
    }

    @DisplayName("Testa não pode publicar vídeo com mais de 100 caracteres no título")
    @Test
    void naoDevePublicarVideoSeTituloTiverMaisCaracteres() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioProfessor();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"" + "a".repeat(101) + "\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("titulo")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("Título deve possuir até 100 caracteres")))
        ;
    }

    @DisplayName("Testa não pode publicar vídeo com mais de 255 caracteres na descrição")
    @Test
    void naoDevePublicarVideoSeDescricaoTiverMaisCaracteres() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"descricao\": \"" + "a".repeat(256) + "\", " +
                                                "\"titulo\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("descricao")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("Descrição deve possuir até 255 caracteres")))
        ;
    }

    @DisplayName("Testa não pode publicar vídeo com url inválida")
    @Test
    void naoDevePublicarVideoSeURLForInvalida() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"descricao\": \"" + "a".repeat(255) + "\", " +
                                                "\"titulo\": \"" + "b".repeat(100) + "\", " +
                                                "\"url\": \"abc\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].campo",
                        Matchers.is("url")))
                .andExpect(jsonPath("$[0].mensagem",
                        Matchers.is("URL deve ser válida")))
        ;
    }

    @DisplayName("Testa usuário não pode sobrescrever data de publicação")
    @Test
    void naoDevePublicarDataDePublicacaoInformadaEmVideo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"dataPublicacao\": \"2000-01-01T00:00:00\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                .andExpect(request().asyncStarted())
                .andReturn();

                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.dataPublicacao",
                        Matchers.not(containsString("2000-01-01"))))
                .andExpect(jsonPath("$.dataPublicacao",
                        containsString(LocalDateTime.now().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa usuário não pode sobrescrever id")
    @Test
    void naoDevePublicarIdInformadoEmVideo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioProfessor();
        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        post(ENDPOINT)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"id\": \"ID_INVENTADO\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                .andExpect(request().asyncStarted())
                .andReturn();
                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"))
                .andExpect(header().string("Location", Matchers.not(containsString("ID_INVENTADO"))))
                ;

    }

    @DisplayName("Testa usuário com permissão pode visualizar vídeo")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveVisualizarVideoSeAluno(
            String login,
            String senha,
            PapelUsuario papel
    ) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoCadastrado = this.getVideoCadastrado();

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT + "/" + videoCadastrado.getId())
                                .with(user(usuarioLogado))
                )
                .andExpect(request().asyncStarted())
                .andReturn();
                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo",
                        Matchers.is(videoCadastrado.getTitulo())))
                .andExpect(jsonPath("$.descricao",
                        Matchers.is(videoCadastrado.getDescricao())))
                .andExpect(jsonPath("$.url",
                        Matchers.is(videoCadastrado.getUrl())))
                .andExpect(jsonPath("$.dataPublicacao",
                        Matchers.containsString(videoCadastrado.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa não deve visualizar vídeo se foi inativado")
    @Test
    void naoDeveVisualizarVideoSeInativo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        var videoCadastrado = this.getVideoCadastrado();
        videoCadastrado.inativa();
        videoRepository.save(videoCadastrado).toFuture();

        // Act
        this.mockMvc.perform(
                        get(ENDPOINT + "/" + videoCadastrado.getId())
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.mensagem",
                        Matchers.is(
                                "Não foi possível encontrar vídeo com o código '" +
                                        videoCadastrado.getId() +
                                        "' informado")))
                ;
    }

    @DisplayName("Testa deve informar erro se codigo incorreto")
    @Test
    void naoDeveVisualizarVideoSeCodigoInexistente() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();

        // Act
        this.mockMvc.perform(
                        get(ENDPOINT + "/123")
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.mensagem",
                        Matchers.is("Não foi possível encontrar vídeo com o código '123' informado")))
        ;
    }

    @DisplayName("Testa usuários com permissão podem listar vídeos")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveListarVideosSeUsuarioComPermissao(
            String login,
            String senha,
            PapelUsuario papel) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoCadastrado = this.getVideoCadastrado();

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT)
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(request().asyncStarted())
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body.content",
                        hasSize(1)))
                .andExpect(jsonPath("$[0].body.content[0].titulo",
                        Matchers.is(videoCadastrado.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[0].descricao",
                        Matchers.is(videoCadastrado.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[0].url",
                        Matchers.is(videoCadastrado.getUrl())))
                .andExpect(jsonPath("$[0].body.content[0].dataPublicacao",
                        Matchers.containsString(videoCadastrado.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa deve informar erro se repositório vazio")
    @Test
    void naoDeveListarVideoSeCodigoInexistente() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();

        // Act
        this.mockMvc.perform(
                        get(ENDPOINT)
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.mensagem",
                        Matchers.is("Não há vídeos disponíveis para visualização")))
        ;
    }

    @DisplayName("Testa não deve listar vídeo inativo")
    @Test
    void naoDeveListarVideoInativo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        var videoCadastrado = this.getVideoCadastrado();
        videoCadastrado.inativa();
        videoRepository.save(videoCadastrado).toFuture();

        // Act
        this.mockMvc.perform(
                        get(ENDPOINT)
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.mensagem",
                        Matchers.is("Não há vídeos disponíveis para visualização")))
        ;
    }

    @DisplayName("Testa deve listar vídeos por ordem decrescente de data de publicação no default")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveListarVideosParaTodosUsuarios(
            String login,
            String senha,
            PapelUsuario papel) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoMaisAntigo = this.getVideoParametrizado(
                "Título 1",
                "Descrição 1",
                "https://www.vipassei.com.br/video1");
        var videoMaisNovo = this.getVideoParametrizado(
                "Título 2",
                "Descrição 2",
                "https://www.vipassei.com.br/video2");

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT)
                                .with(user(usuarioLogado))
                )
                .andExpect(request().asyncStarted())
                .andReturn();

                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body.content",
                        hasSize(2)))
                .andExpect(jsonPath("$[0].body.content[0].titulo",
                        Matchers.is(videoMaisNovo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[0].descricao",
                        Matchers.is(videoMaisNovo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[0].url",
                        Matchers.is(videoMaisNovo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[0].dataPublicacao",
                        Matchers.containsString(videoMaisNovo.getDataPublicacao().toLocalDate().toString())))
                .andExpect(jsonPath("$[0].body.content[1].titulo",
                        Matchers.is(videoMaisAntigo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[1].descricao",
                        Matchers.is(videoMaisAntigo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[1].url",
                        Matchers.is(videoMaisAntigo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[1].dataPublicacao",
                        Matchers.containsString(videoMaisAntigo.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa deve listar vídeos permitindo ordenar por data de publicacao")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveListarVideosComOrdenacaoPassadaPeloUsuario(
            String login,
            String senha,
            PapelUsuario papel) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoMaisAntigo = this.getVideoParametrizado(
                "Título 1",
                "Descrição 1",
                "https://www.vipassei.com.br/video1");
        var videoMaisNovo = this.getVideoParametrizado(
                "Título 2",
                "Descrição 2",
                "https://www.vipassei.com.br/video2");

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT)
                                .param("sort", "dataPublicacao")
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(request().asyncStarted())
                .andReturn();

        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body.content",
                        hasSize(2)))
                .andExpect(jsonPath("$[0].body.content[0].titulo",
                        Matchers.is(videoMaisAntigo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[0].descricao",
                        Matchers.is(videoMaisAntigo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[0].url",
                        Matchers.is(videoMaisAntigo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[0].dataPublicacao",
                        Matchers.containsString(videoMaisAntigo.getDataPublicacao().toLocalDate().toString())))
                .andExpect(jsonPath("$[0].body.content[1].titulo",
                        Matchers.is(videoMaisNovo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[1].descricao",
                        Matchers.is(videoMaisNovo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[1].url",
                        Matchers.is(videoMaisNovo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[1].dataPublicacao",
                        Matchers.containsString(videoMaisNovo.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa deve listar vídeos permitindo filtro por título")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveListarVideosComFiltroPorTitulo(
            String login,
            String senha,
            PapelUsuario papel) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoMaisAntigo = this.getVideoParametrizado(
                "Título 1",
                "Descrição 1",
                "https://www.vipassei.com.br/video1");
        var videoMaisNovo = this.getVideoParametrizado(
                "Título 2",
                "Descrição 2",
                "https://www.vipassei.com.br/video2");

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT)
                                .param("titulo", "Título 2")
                                .with(user(usuarioLogado))
                )
                .andExpect(request().asyncStarted())
                .andReturn();
                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body.content",
                        hasSize(1)))
                .andExpect(jsonPath("$[0].body.content[0].titulo",
                        Matchers.is(videoMaisNovo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[0].descricao",
                        Matchers.is(videoMaisNovo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[0].url",
                        Matchers.is(videoMaisNovo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[0].dataPublicacao",
                        Matchers.containsString(videoMaisNovo.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa deve listar vídeos permitindo filtro por data de publicação")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoGET")
    void deveListarVideosComFiltroPorPublicacao(
            String login,
            String senha,
            PapelUsuario papel) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoMaisAntigo = this.getVideoParametrizado(
                "Título 1",
                "Descrição 1",
                "https://www.vipassei.com.br/video1",
                LocalDateTime.of(2022, 12, 31, 0, 0));
        var videoMaisNovo = this.getVideoParametrizado(
                "Título 2",
                "Descrição 2",
                "https://www.vipassei.com.br/video2",
                LocalDateTime.of(2023, 1, 1, 0, 0));

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        get(ENDPOINT)
                                .param("dataPublicacao",
                                        LocalDate.of(2022, 12, 31).toString())
                                .with(user(usuarioLogado))
                )
                .andExpect(request().asyncStarted())
                .andReturn();

                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].body.content",
                        hasSize(1)))
                .andExpect(jsonPath("$[0].body.content[0].titulo",
                        Matchers.is(videoMaisAntigo.getTitulo())))
                .andExpect(jsonPath("$[0].body.content[0].descricao",
                        Matchers.is(videoMaisAntigo.getDescricao())))
                .andExpect(jsonPath("$[0].body.content[0].url",
                        Matchers.is(videoMaisAntigo.getUrl())))
                .andExpect(jsonPath("$[0].body.content[0].dataPublicacao",
                        Matchers.containsString(videoMaisAntigo.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa usuários sem permissão para remover recebem mensagem de erro adequada")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosSemPermissaoDELETE")
    void deveNaoPodeRemoverVideoSeAlunoOuProfessor(
            String login,
            String senha,
            PapelUsuario papel
    ) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var video = this.getVideoCadastrado();

        // Act
        this.mockMvc.perform(
                        delete(ENDPOINT + "/" + video.getId())
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isForbidden())
        ;
    }

    @DisplayName("Testa usuário admin pode remover vídeo")
    @Test
    void deveRemoverVideoSeUsuarioAdmin() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        var video = this.getVideoCadastrado();

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        delete(ENDPOINT + "/" + video.getId())
                                .with(user(usuarioLogado))
                )
                .andExpect(request().asyncStarted())
                .andReturn();
                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isNoContent());
    }

    @DisplayName("Testa retorna erro não encontrado para tentativa de remover id inexistente")
    @Test
    void deveInformarNaoEncontradoSeNaoIdentificaVideoARemover() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();

        // Act
        this.mockMvc.perform(
                        delete(ENDPOINT + "/abcde")
                                .with(user(usuarioLogado))
                )
                // Assert
                .andExpect(status().isNotFound());

    }

    @DisplayName("Testa aluno não pode editar video")
    @Test
    void naoDeveEditarVideoSeAluno() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAluno();
        var videoAEditar = this.getVideoCadastrado();

        // Act
        this.mockMvc.perform(
                        put(ENDPOINT + "/" + videoAEditar.getId())
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"" + videoAEditar.getTitulo() + "A\", " +
                                                "\"descricao\": \"" + videoAEditar.getDescricao() + "A\", " +
                                                "\"url\": \"" + videoAEditar.getUrl() + "A\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa usuário com permissão pode editar video")
    @ParameterizedTest
    @MethodSource("testDadosUsuariosComPermissaoPOSTePUT")
    void deveEditarVideoSePossuiPermissao(
            String login,
            String senha,
            PapelUsuario papel
    ) throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioParametrizado(login, senha, papel);
        var videoAEditar = this.getVideoCadastrado();

        // Act
        MvcResult mvcResult = this.mockMvc.perform(
                        put(ENDPOINT + "/" + videoAEditar.getId())
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"" + videoAEditar.getTitulo() + "A\", " +
                                                "\"descricao\": \"" + videoAEditar.getDescricao() + "A\", " +
                                                "\"url\": \"" + videoAEditar.getUrl() + "A\"}"
                                )
                )
                .andExpect(request().asyncStarted())
                .andReturn();

                // Assert
        this.mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo",
                        Matchers.is(videoAEditar.getTitulo() + "A")))
                .andExpect(jsonPath("$.descricao",
                        Matchers.is(videoAEditar.getDescricao() + "A")))
                .andExpect(jsonPath("$.url",
                        Matchers.is(videoAEditar.getUrl() + "A")))
                .andExpect(jsonPath("$.dataPublicacao",
                        Matchers.containsString(videoAEditar.getDataPublicacao().toLocalDate().toString())))
        ;
    }

    @DisplayName("Testa não pode editar vídeo inativo")
    @Test
    void naoDeveEditarVideoSeInativo() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioProfessor();
        var videoAEditar = this.getVideoCadastrado();
        videoAEditar.inativa();
        videoRepository.save(videoAEditar).toFuture();

        // Act
        this.mockMvc.perform(
                        put(ENDPOINT + "/" + videoAEditar.getId())
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"" + videoAEditar.getTitulo() + "A\", " +
                                                "\"descricao\": \"" + videoAEditar.getDescricao() + "A\", " +
                                                "\"url\": \"" + videoAEditar.getUrl() + "A\"}"
                                )
                )
                // Assert
                .andExpect(status().isNotFound())
        ;
    }

    private Usuario getUsuarioParametrizado(
            String login,
            String senhaBruta,
            PapelUsuario papelUsuario
    ){
        String senhaCriptografada = passwordEncoder.encode(senhaBruta);
        return usuarioRepository.save(
                new Usuario(login, senhaCriptografada, papelUsuario)
        );
    }

    private Usuario getUsuarioAluno(){
        String senhaCriptografada = passwordEncoder.encode("123456");
        return usuarioRepository.save(
                new Usuario("aluno", senhaCriptografada, PapelUsuario.ALUNO)
        );
    }

    private Usuario getUsuarioAdmin(){
        String senhaCriptografada = passwordEncoder.encode("654321");
        return usuarioRepository.save(
                new Usuario("admin", senhaCriptografada, PapelUsuario.ADMIN)
        );
    }

    private Usuario getUsuarioProfessor(){
        String senhaCriptografada = passwordEncoder.encode("02468");
        return usuarioRepository.save(
                new Usuario("professor", senhaCriptografada, PapelUsuario.PROFESSOR)
        );
    }

    private Video getVideoCadastrado() throws ExecutionException, InterruptedException {
        return videoRepository.save(
           new Video(
                   "Aula de Design Patterns",
                   "Aprofundando para discursivas",
                   "https://www.vipassei.com.br/auladesignpatterns")
        ).toFuture().get();
    }

    private Video getVideoParametrizado(
            String titulo,
            String descricao,
            String url
    ) throws ExecutionException, InterruptedException {
        return videoRepository.save(
                new Video(titulo, descricao, url)
        ).toFuture().get() ;
    }

    private Video getVideoParametrizado(
            String titulo,
            String descricao,
            String url,
            LocalDateTime dataPublicacao
    ) throws ExecutionException, InterruptedException {
        return videoRepository.save(
                new Video(titulo, descricao, url, dataPublicacao)
        ).toFuture().get() ;
    }

    static Stream<Arguments> testDadosUsuariosComPermissaoGET(){
        return Stream.of(
                Arguments.of("aluno", "123456", PapelUsuario.ALUNO),
                Arguments.of("professor", "123456", PapelUsuario.PROFESSOR),
                Arguments.of("admin", "123456", PapelUsuario.ADMIN)
        );
    }

    static Stream<Arguments> testDadosUsuariosComPermissaoPOSTePUT(){
        return Stream.of(
                Arguments.of("professor", "123456", PapelUsuario.PROFESSOR),
                Arguments.of("admin", "123456", PapelUsuario.ADMIN)
        );
    }

    static Stream<Arguments> testDadosUsuariosSemPermissaoDELETE(){
        return Stream.of(
                Arguments.of("aluno", "123456", PapelUsuario.ALUNO),
                Arguments.of("professor", "123456", PapelUsuario.PROFESSOR)
        );
    }

}