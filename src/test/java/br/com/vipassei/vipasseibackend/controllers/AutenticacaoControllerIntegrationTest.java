package br.com.vipassei.vipasseibackend.controllers;

import br.com.vipassei.vipasseibackend.domain.usuario.PapelUsuario;
import br.com.vipassei.vipasseibackend.domain.usuario.Usuario;
import br.com.vipassei.vipasseibackend.exceptions.AutenticacaoException;
import br.com.vipassei.vipasseibackend.infrastructure.security.TokenService;
import br.com.vipassei.vipasseibackend.repositories.UsuarioRepository;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.contains;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AutenticacaoControllerIntegrationTest {


    private final String ENDPOINT_REGISTRO = "/auth/registrar";

    private final String ENDPOINT_LOGIN = "/auth/login";

    private final String ENDPOINT_VIDEOAULAS = "/videoaulas";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenService tokenService;

    @AfterEach
    public void tearDown(){
        mongoTemplate.getDb().drop();
    }

    @DisplayName("Testa que nao deve garantir acesso a usuario valido sem autenticacao")
    @Test
    void naoDeveAcessarEndpointValidoSemAutenticacaoMesmoUsuarioValido() throws Exception {
        // Arrange
        var usuarioLogado = getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_VIDEOAULAS)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa que deve garantir acesso a usuario valido com autenticacao")
    @Test
    void deveAcessarEndpointValidoComAutenticacaoUsuarioValido() throws Exception {
        // Arrange
        var usuarioLogado = getUsuarioAdmin();
        var token = tokenService.geraToken(usuarioLogado);
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_VIDEOAULAS)
                                .header("Authorization", "Bearer " + token)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isOk())
                .andExpect(request().asyncStarted());

    }

    @DisplayName("Testa que não deve garantir acesso a usuario valido informando token errado")
    @Test
    void naoDeveAcessarEndpointValidoSemAutenticacaoValidaDeUsuario() throws Exception {
        // Arrange
        var usuarioLogado = getUsuarioAdmin();
        var token = tokenService.geraToken(usuarioLogado);
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_VIDEOAULAS)
                                .header("Authorization", "Bearer token-incorreto")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"titulo\": \"Aula com metodologia\", " +
                                                "\"descricao\": \"Fundamentada em anos de pesquisa\", " +
                                                "\"url\": \"http://www.videovalidado.com\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa que não deve gerar Token para usuário nulo")
    @Test
    void naoDeveGerarTokenParaUsuarioNulo() throws Exception {
        // Act & Assert
        assertThrows(
                AutenticacaoException.class,
                () -> tokenService.geraToken(null)
        );
    }

    @DisplayName("Testa que Aluno não deve registrar usuario")
    @Test
    void deveNaoRegistrarNovoUsuarioSeAluno() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAluno();
        // Act
        this.mockMvc.perform(
                post(ENDPOINT_REGISTRO)
                        .with(user(usuarioLogado))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\"login\": \"Meu amigo\", " +
                                "\"senha\": \"minhasenha\", " +
                                "\"papel\": \"ADMIN\"}"
                        )
        )
        // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa que Professor não deve registrar usuario")
    @Test
    void deveNaoRegistrarNovoUsuarioSeProfessor() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioProfessor();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_REGISTRO)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"professora\", " +
                                                "\"senha\": \"amigadefaculdade\", " +
                                                "\"papel\": \"PROFESSOR\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    @DisplayName("Testa que Admin pode registrar usuario")
    @Test
    void deveRegistrarNovoUsuarioSeAdmin() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_REGISTRO)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"Professor Girafalis\", " +
                                                "\"senha\": \"donaflorinda\", " +
                                                "\"papel\": \"PROFESSOR\"}"
                                )
                )
                // Assert
                .andExpect(status().isOk());
    }


    @DisplayName("Testa que Admin não pode registrar usuario mais de uma vez")
    @Test
    void naoDeveRegistrarUsuarioNovamente() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        var usuarioJaRegistrado = this.getUsuarioAluno();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_REGISTRO)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"" + usuarioJaRegistrado.getUsername() + "\", " +
                                                "\"senha\": \"" + usuarioJaRegistrado.getPassword() + "\", " +
                                                "\"papel\": \"" + usuarioJaRegistrado.getPapel().getPapel() + "\"}"
                                )
                )
                // Assert
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.mensagem",
                        Matchers.is("Login '"
                                + usuarioJaRegistrado.getUsername()
                                + "' já utilizado")))

        ;
    }

    @DisplayName("Testa que Admin não pode registrar usuario com papel inexistente")
    @Test
    void naoDeveRegistrarUsuarioComPapelNaoMapeado() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        var usuarioJaRegistrado = this.getUsuarioAluno();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_REGISTRO)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"novousuario\", " +
                                                "\"senha\": \"novasenha\", " +
                                                "\"papel\": \"PAPEL_NAO_MAPEADO\"}"
                                )
                )
                // Assert
                .andExpect(status().isBadRequest())

        ;
    }

    @DisplayName("Testa que consegue logar com senha correta")
    @Test
    void deveLogarComSenhaCorreta() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAdmin();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_LOGIN)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"" + usuarioLogado.getUsername() + "\", " +
                                                "\"senha\": \"654321\"}"
                                )
                )
                // Assert
                .andExpect(status().isOk());
    }

    @DisplayName("Testa que não consegue logar com senha incorreta")
    @Test
    void naoDeveLogarComSenhaIncorreta() throws Exception {
        // Arrange
        var usuarioLogado = this.getUsuarioAluno();
        // Act
        this.mockMvc.perform(
                        post(ENDPOINT_LOGIN)
                                .with(user(usuarioLogado))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{\"login\": \"" + usuarioLogado.getUsername() + "\", " +
                                                "\"senha\": \"654321\"}"
                                )
                )
                // Assert
                .andExpect(status().isForbidden());
    }

    private Usuario getUsuarioAluno(){
        String senhaCriptografada = passwordEncoder.encode("123456");
        return usuarioRepository.save(
                new Usuario("aluno", senhaCriptografada, PapelUsuario.ALUNO)
        );
    }

    private Usuario getUsuarioAdmin(){
        String senhaCriptografada = passwordEncoder.encode("654321");
        return usuarioRepository.save(
                new Usuario("admin", senhaCriptografada, PapelUsuario.ADMIN)
        );
    }

    private Usuario getUsuarioProfessor(){
        return usuarioRepository.save(
                new Usuario("professor", "02468", PapelUsuario.PROFESSOR)
        );
    }
}