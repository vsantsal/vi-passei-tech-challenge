package br.com.vipassei.vipasseibackend.domain.video;

import br.com.vipassei.vipasseibackend.exceptions.URLInvalidaException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

class VideoUnitTest {

    private static final String TITULO_DEFAULT = "Título";

    private static final String DESCRICAO_DEFAULT = "Descrição";

    private static final String URL_DEFAULT = "https://www.vipassei.com.br/videoaula00";

    @DisplayName("Testa dia de publicacao de vídeo corresponde ao dia da instanciação")
    @Test
    void devePossuirDataDePublicacaoIgualADataDaCriacao(){
        // Arrange
        LocalDate dataPublicacaoEsperada = LocalDate.now();
        String urlValida = "https://www.vipassei.com.br";
        Video video = new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, urlValida);

        // Act
        LocalDate dataPublicacaoObtida = video.getDataPublicacao().toLocalDate();

        // Assert
        assertThat(dataPublicacaoObtida).isEqualTo(dataPublicacaoEsperada);
    }

    @DisplayName("Testa video não aceita urls inválidas no set")
    @ParameterizedTest
    @ValueSource(strings = {
                            "abc",
                            "https://www.vipassei.com.br/ java-%%$^&& iuyi",
                            "http://www.vipassei.com.br/ .html",
                            "http:\\www.vipassei.com.br",
                            "/main.html"
    })
    void deveNaoAceitarURLsInvalidasNoSetter(String url){
        // Arrange
        Video video = new Video();

        // Act & Assert
        assertThatThrownBy(
                () -> video.setUrl(url)
        ).isInstanceOf(URLInvalidaException.class);

    }

    @DisplayName("Testa video não aceita urls inválidas no construtor")
    @ParameterizedTest
    @ValueSource(strings = {
            "abc",
            "https://www.vipassei.com.br/ java-%%$^&& iuyi",
            "http://www.vipassei.com.br/ .html",
            "http:\\www.vipassei.com.br",
            "/main.html"
    })
    void deveNaoAceitarURLsInvalidasNoConstrutor(String url){
        // Arrange, Act & Assert
        assertThatThrownBy(
                () -> new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, url)
        ).isInstanceOf(URLInvalidaException.class);

    }

    @DisplayName("Testa vídeo deve estar ativo após instanciação por qualquer construtor")
    @ParameterizedTest
    @MethodSource("dadosVideosDeTeste")
    void deveEstarAtivoAposInstanciacao(Video video) {
        // Act
        boolean estahAtivo = video.isAtivo();

        // Assert
        assertThat(estahAtivo).isTrue();
    }

    @DisplayName("Testa não fica ativo após inativação")
    @Test
    void deveNaoEstarAtivoAposInativar(){
        // Arrange
        LocalDateTime dataPublicacaoInformada = LocalDateTime.of(2023, 1, 1, 10, 30);
        Video video = new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, URL_DEFAULT, dataPublicacaoInformada);

        // Act
        video.inativa();

        // Assert
        assertThat(video.isAtivo()).isFalse();
        assertThat(video.getDataUltimaAtualizacao()).isAfter(dataPublicacaoInformada);
    }

    @DisplayName("Testa não inativa vídeo inativo")
    @Test
    void deveNaoInativarVideoInativo(){
        // Arrange
        Video video = new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, URL_DEFAULT);
        video.inativa();
        LocalDateTime dataPrimeiraInativacao = video.getDataUltimaAtualizacao();

        // Act
        video.inativa();

        // Assert
        assertThat(video.isAtivo()).isFalse();
        assertThat(video.getDataUltimaAtualizacao()).isEqualTo(dataPrimeiraInativacao);
    }

    static Stream<Arguments> dadosVideosDeTeste(){
        return Stream.of(
                Arguments.of(new Video()),
                Arguments.of(new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, URL_DEFAULT)),
                Arguments.of(new Video(TITULO_DEFAULT, DESCRICAO_DEFAULT, URL_DEFAULT, LocalDateTime.now()))
        );
    }


}